<?php
declare(strict_types=1);
namespace SocialCoversGenerator;

class Loader
{

    public function __construct(array $data = [])
    {
        foreach ($data as $func => $value) {
            $func_name = $this->getFuncByString($func, 'set');

            if (\method_exists($this, $func_name)) {
                $this->{$func_name}($value);
            }
        }
    }

    public function getFuncByString(string $field, string $prefix = '')
    {
        return sprintf('%s%s', $prefix, implode('', array_map('ucfirst', explode('_', $field))));
    }

}