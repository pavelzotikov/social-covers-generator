<?php
declare(strict_types=1);
namespace SocialCoversGenerator\Types;

class Image extends AbstractType
{

    protected $path = '';
    protected $fill = '';

    protected $round_corners = 0;

    public function getImage(bool $no_resize = false): \Imagick
    {
        if ($this->layer) {
            return $this->layer;
        }

        $layer = new \Imagick();
        $layer->readImage($this->getPath());

        if (!$no_resize && $this->getWidth() && $this->getHeight()) {
            $layer->adaptiveResizeImage($this->getWidth(), $this->getHeight(), false);
        }

        if ($this->getRoundCorners()) {
            $layer->roundCorners($this->getRoundCorners(), $this->getRoundCorners());
        }

        if ($this->getFill()) {
            $layer_colorize = new \Imagick();
            $layer_colorize->newImage($layer->getImageWidth(), $layer->getImageHeight(), $this->getFill());
            $layer_colorize->compositeImage($layer, \Imagick::COMPOSITE_COPYOPACITY, 0, 0);
            $layer = $layer_colorize;
        }

        $this->layer = $layer;

        return $layer;
    }

    public function setFill(string $fill): self
    {
        $this->fill = $fill;

        return $this;
    }

    public function setWidth(int $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function setHeight(int $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function setRoundCorners(int $value): self
    {
        $this->round_corners = $value;

        return $this;
    }

    public function getWidth(): ?int
    {
        return $this->width;
    }

    public function getHeight(): ?int
    {
        return $this->height;
    }

    public function getFill(): ?string
    {
        return $this->fill;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getRoundCorners(): int
    {
        return $this->round_corners;
    }

}